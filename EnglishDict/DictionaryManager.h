//
//  DictionaryManager.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 10/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DictionaryManager : NSObject

@property (nonatomic,strong) NSArray *dictionary;
@property (nonatomic,strong) NSMutableDictionary *hashDictionary;
@property (nonatomic)        double searchDelay;

+ (id)sharedManager;
- (void)fetchDictionary;
- (void)fetchDictionaryWithIndex;
- (NSArray *)filterWithGivenText:(NSString *)searchText;



@end
