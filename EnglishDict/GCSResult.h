//
//  GCSResult.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCSItem.h"
#import <Mantle/Mantle.h>

@interface GCSResult : MTLModel <MTLJSONSerializing>

@property (nonatomic,strong) NSArray *items;

@end
