//
//  GCSItem.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface GCSItem :  MTLModel <MTLJSONSerializing>

@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *link;
@property (nonatomic,strong) NSString *snippet;

@end
