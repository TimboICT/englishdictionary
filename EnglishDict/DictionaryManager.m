//
//  DictionaryManager.m
//  EnglishDict
//
//  Created by Arda Doğantemur on 10/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "DictionaryManager.h"
#import "EDValues.h"

@implementation DictionaryManager

#pragma mark - Shared Manager
//========================================================================
+ (id)sharedManager
{
    static DictionaryManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      sharedManager = [[self alloc] init];
                  });
    return sharedManager;
}

#pragma mark - Constructor
//========================================================================
- (instancetype)init
{
    self = [super init];
    if (self)
    {

    }
    return self;
}

#pragma mark - Fetcher
//========================================================================
- (void)fetchDictionary
{
    [DELEGATE setAnimationText:@"Loading Words"];
    _dictionary = [NSMutableArray new];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[EDValues dictionaryFileName] ofType:@""];
    
    //decomposedStringWithCanonicalMapping for Cononical Mapping
    NSString *content = [[[NSString alloc] initWithData:[NSData dataWithContentsOfFile:filePath] encoding:NSUTF8StringEncoding] decomposedStringWithCanonicalMapping];

    
    [DELEGATE setAnimationText:@"Sorting Words"];

    NSArray *array  = [[content componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    _dictionary = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
    
    
    [self fetchDictionaryWithIndex];
}

- (void)fetchDictionaryWithIndex
{
    _hashDictionary = [[NSUserDefaults standardUserDefaults] valueForKey:@"hashDictionary"];
    if(!_hashDictionary)
    {
        _hashDictionary = [[NSMutableDictionary alloc] init];
        [DELEGATE setAnimationText:@"Indexing"];
        [self convertDictionaryToHashDictionary];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dictionaryFetched" object:self];
    }
}


#pragma mark - Search Algorithms
//========================================================================
- (NSArray *)filterWithGivenText:(NSString *)searchText
{
   return [self filterWithGivenTextWithHex:searchText];
}

- (NSArray *)filterWithGivenTextWithPredicate:(NSString *)searchText Source:(NSArray *)dictionary
{
    if(dictionary.count == 0)
    {
        dictionary = _dictionary;
    }
    NSArray *filtered = [dictionary filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF BEGINSWITH[cd] %@)",searchText]];

    return filtered;
}

- (NSArray *)filterWithGivenTextWithHex:(NSString *)searchText
{
    NSMutableDictionary *dict = _hashDictionary;
    for (int i= 0; i<searchText.length && i<[EDValues hashDepth]; i++)
    {
        NSString *character = [searchText substringWithRange:NSMakeRange(i, 1)];
        dict = [dict valueForKey:[character lowercaseString]];
    }
    
    int startIndex = [[dict valueForKey:@"startIndex"] intValue];
    int endIndex = [[dict valueForKey:@"endIndex"] intValue];
    
    if(searchText.length<=[EDValues hashDepth])
    {
        return [_dictionary subarrayWithRange:NSMakeRange(startIndex, endIndex-startIndex)];
    }
    else
    {
        return [self filterWithGivenTextWithPredicate:searchText Source:[_dictionary subarrayWithRange:NSMakeRange(startIndex, endIndex-startIndex)]];
    }
}


#pragma mark - Dictionary Tree
//========================================================================
- (void)convertDictionaryToHashDictionary
{
    int counter = 0;
    for (NSString *word in _dictionary)
    {
        NSMutableDictionary *tempDict = _hashDictionary;
        
        for(int i=0; i<(word.length) && i<[EDValues hashDepth]; i++)
        {
            NSString *character = [word substringWithRange:NSMakeRange(i, 1)];
            character = [character lowercaseString];
            
            if([tempDict valueForKey:character]==nil)
            {
                NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:counter],@"startIndex",[NSNumber numberWithInt:counter],@"endIndex", nil];
                [tempDict setValue:dictionary forKey:character];
                tempDict = [tempDict valueForKey:character];
            }
            else
            {

                tempDict = [tempDict valueForKey:character];
                int endIndex = [[tempDict valueForKey:@"endIndex"] intValue];
                endIndex ++;
                [tempDict setValue:[NSNumber numberWithInt:endIndex] forKey:@"endIndex"];
            }
        }
        
        counter ++;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:_hashDictionary forKey:@"hashDictionary"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"dictionaryFetched" object:self];
}


#pragma mark - Experitment
//========================================================================
//- (NSArray *)filterWithGivenTextWithCustom:(NSString *)searchText Dictionary:(NSArray *)dictionary CharIndex:(int)index
//{
//    if(searchText.length >0)
//    {
//        NSString *character = [searchText substringWithRange:NSMakeRange(0, 1)];
//        int startIndex = -1;
//        int endIndex = -1;
//        
//        
//        for (int i=0; i<dictionary.count; i++)
//        {
//            NSString *dictionaryWord = [dictionary objectAtIndex:i];
//            if(dictionaryWord.length > index)
//            {
//                NSString *fChar = [dictionaryWord substringWithRange:NSMakeRange(index, 1)];
//                
//                if([[fChar lowercaseString] isEqualToString:[character lowercaseString]])
//                {
//                    if(startIndex == -1)
//                    {
//                        startIndex = i;
//                    }
//                }
//                else
//                {
//                    if(startIndex != -1)
//                    {
//                        endIndex = i;
//                        break;
//                    }
//                }
//            }
//            
//        }
//        
//        if(endIndex==-1)
//        {
//            endIndex = (int)dictionary.count;
//        }
//        
//        if(startIndex!=-1)
//        {
//            NSArray *filteredArray = [dictionary subarrayWithRange:NSMakeRange(startIndex, endIndex-startIndex)];
//            
//            if(searchText.length == 1)
//            {
//                return filteredArray;
//            }
//            else
//            {
//                NSString *searchTextModified = [searchText substringWithRange:NSMakeRange(1, searchText.length-1)];
//                return [self filterWithGivenTextWithCustom:searchTextModified Dictionary:filteredArray CharIndex:++index];
//            }
//        }
//        else
//        {
//            return [NSArray new];
//        }
//    }
//    else
//    {
//        return nil;
//    }
//    
//}

@end
