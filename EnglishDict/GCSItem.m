//
//  GCSItem.m
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "GCSItem.h"

@implementation GCSItem

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    NSMutableDictionary *paths = [[NSMutableDictionary alloc] init];
    
    [paths addEntriesFromDictionary:@{
                                      @"title":@"title",
                                      @"link":@"link",
                                      @"snippet":@"snippet"
                                      }];
    return paths;
}


@end
