//
//  DetailViewController.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCSItem.h"
@interface DetailViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *detailWebView;
@property (nonatomic,strong) GCSItem *item;

@end
