//
//  DetailTableViewCell.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCSItem.h"
@interface DetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *linkLabel;
- (void)prepareCell:(GCSItem *)item;

@end
