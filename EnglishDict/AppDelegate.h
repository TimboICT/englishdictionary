//
//  AppDelegate.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 10/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReSideMenu.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) RESideMenu *sideMenuViewController;

- (void)enableLoadingActivity;
- (void)disableLoadingActivity;
- (void)setAnimationText:(NSString *)text;

@end

