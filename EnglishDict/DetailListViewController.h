//
//  DetailViewController.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCSResult.h"
@interface DetailListViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) GCSResult *result;
@property (nonatomic,strong) GCSItem *selectedItem;

@end
