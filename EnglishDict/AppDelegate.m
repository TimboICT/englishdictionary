//
//  AppDelegate.m
//  EnglishDict
//
//  Created by Arda Doğantemur on 10/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "AppDelegate.h"
#import "LeftMenuViewController.h"
#import "MainViewController.h"
#import "RTSpinKitView.h"
#import "ViewUtils.h"
@interface AppDelegate ()
@property (nonatomic,strong) RTSpinKitView *spinner;
@property (nonatomic,strong) UIView *spinnerView;
@property (nonatomic,strong) UILabel *spinnerLabel;
@property (nonatomic) int spinCount;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [self createSpinner];


    UINavigationController *navigationController =     [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController];
    LeftMenuViewController *leftMenuViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"leftMenuViewController"];

    _sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                    leftMenuViewController:leftMenuViewController
                                                                   rightMenuViewController:nil];
    
    // Make it a root controller
    //
    self.window.rootViewController = _sideMenuViewController;
    
    [self fetchDictionary];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Loading Animation
//========================================================================
- (void)enableLoadingActivity
{
    if(_spinCount==0)
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           UIWindow *keyWindow =  self.window;
                           _spinnerView.frame =keyWindow.bounds;
                           
                           _spinner.center = _spinnerView.center;
                           _spinnerLabel.center = _spinnerView.center;
                           _spinnerLabel.y = _spinner.bottom+20;
                           [keyWindow addSubview:_spinnerView];
                       });
    }
    _spinCount++;
}

- (void)disableLoadingActivity
{
    if(_spinCount>0)
    {
        _spinCount--;
        if(_spinCount==0)
        {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [self setAnimationText:@""];
                               [_spinnerView removeFromSuperview];
                           });
        }
    }
}

- (void)createSpinner
{
    _spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWanderingCubes color:[UIColor colorWithRed:0.0/255.0 green:164.0/255.0 blue:235.0/255.0 alpha:1.0]];
    _spinnerView = [[UIView alloc] init];
    
    _spinnerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 20)];
    _spinnerLabel.textColor = [UIColor whiteColor];
    [_spinnerLabel setTextAlignment:NSTextAlignmentCenter];
    [_spinnerLabel setBackgroundColor:[UIColor clearColor]];
    
    _spinnerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    _spinCount = 0;
    [_spinner startAnimating];

    [_spinnerView addSubview:_spinner];
    [_spinnerView addSubview:_spinnerLabel];
}

- (void)setAnimationText:(NSString *)text
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [_spinnerLabel setText:text];
                   });
}

#pragma mark - Initial Creation
//========================================================================
- (void)fetchDictionary
{
    DictionaryManager *manager = [DictionaryManager sharedManager];
    
    if(!manager.dictionary)
    {
        [self enableLoadingActivity];
        [self performSelectorInBackground:@selector(fetchDictionaryInBGThread) withObject:nil];
    }
}

- (void)fetchDictionaryInBGThread
{
    DictionaryManager *manager = [DictionaryManager sharedManager];

    [manager fetchDictionary];
}


@end
