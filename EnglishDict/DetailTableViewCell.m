//
//  DetailTableViewCell.m
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "DetailTableViewCell.h"

@implementation DetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareCell:(GCSItem *)item
{
    [_detailLabel setText:item.snippet];
    [_titleLabel setText:item.title];
    [_linkLabel setText:item.link];
}


@end
