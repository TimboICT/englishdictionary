//
//  ViewController.m
//  EnglishDict
//
//  Created by Arda Doğantemur on 10/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "MainViewController.h"
#import "EDTableViewCell.h"
#import "EDValues.h"
#import "AFHTTPRequestOperationManager.h"
#import "DetailListViewController.h"


@interface MainViewController ()

@property (nonatomic,strong) DictionaryManager *manager;
@property (nonatomic,strong) NSTimer *searchDelayTimer;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _manager = [DictionaryManager sharedManager];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dictionaryFetchedNotificaitonCame) name:@"dictionaryFetched" object:nil];
}

#pragma mark - Segue
//========================================================================
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    DetailListViewController *vc = [segue destinationViewController];
    vc.result = _result;
}


#pragma mark - Pre Fetch
//========================================================================
- (void)dictionaryHasedNotificaitonCame
{
    [_tableView reloadData];
    [DELEGATE disableLoadingActivity];
}

- (void)dictionaryFetchedNotificaitonCame
{
    [_tableView reloadData];
    [DELEGATE disableLoadingActivity];
}

#pragma mark - Tableview Delegate Datasource
//================================================================================================
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_tableView)
    {
        return _manager.dictionary.count;
    }
    else
    {
        return [_filteredDictionary count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        static NSString *MyIdentifier = @"SearchResult";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        }
        

        cell.textLabel.text = [_filteredDictionary objectAtIndex:indexPath.row];
        return cell;
    }
    else
    {
        NSString *cellIdentifier = @"edCell";
        EDTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        [cell prepareCell:[_manager.dictionary objectAtIndex:indexPath.row]];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSString *word = [_filteredDictionary objectAtIndex:indexPath.row];
        [self callSearchApi:word];
    }
    else
    {
        NSString *word = [_manager.dictionary objectAtIndex:indexPath.row];
        [self callSearchApi:word];
    }
}

#pragma mark - Service Call
//========================================================================
- (void)callSearchApi:(NSString *)keyword
{
    NSString *url = [NSString stringWithFormat:@"%@?key=%@&cx=%@&q=%@",[EDValues googleCustomSearchURL],[EDValues googleCustomSearchApiKey],[EDValues googleCustomSearchCxKeyGoogle],keyword];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.requestSerializer.timeoutInterval = 10;

    [DELEGATE enableLoadingActivity];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [DELEGATE disableLoadingActivity];
         _result = [MTLJSONAdapter modelOfClass:[GCSResult class] fromJSONDictionary:responseObject error:nil];
         [self performSegueWithIdentifier:@"detailListSegue" sender:self];
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [DELEGATE disableLoadingActivity];
         NSLog(@"Error: %@", error);
     }];
}

#pragma mark - Search Bar
//========================================================================
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    self.searchDisplayController.searchBar.scopeButtonTitles = nil;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text=@"";
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
}

- (void)filterContentForSearchText:(NSString*)searchText
{
    if(_searchDelayTimer)
    {
        [_searchDelayTimer invalidate];
        _searchDelayTimer = nil;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Your main thread code goes in here
        _searchDelayTimer = [NSTimer scheduledTimerWithTimeInterval:_manager.searchDelay target:self selector:@selector(searchWord:) userInfo:searchText repeats:NO];
    });
}

- (void)searchWord:(NSTimer *)timer
{
    NSString *searchText = [timer userInfo];
    _filteredDictionary = [_manager filterWithGivenText:searchText];
    
    NSLog(@"---------------------------------------------------Tagg %lu",(unsigned long)_filteredDictionary.count);
    [self.searchDisplayController.searchResultsTableView reloadData];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self performSelectorInBackground:@selector(filterContentForSearchText:) withObject:searchString];
    self.searchDisplayController.searchBar.scopeButtonTitles = nil;
    
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text]];
    self.searchDisplayController.searchBar.scopeButtonTitles = nil;
    
    return YES;
}

- (IBAction)menuClicked:(id)sender
{
    [DELEGATE.sideMenuViewController presentLeftMenuViewController];
}
@end
