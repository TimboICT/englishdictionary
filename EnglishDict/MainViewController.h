//
//  ViewController.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 10/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DictionaryManager.h"
#import <Mantle/Mantle.h>
#import "GCSResult.h"

@interface MainViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *filteredDictionary;
@property (nonatomic,strong) GCSResult *result;
- (IBAction)menuClicked:(id)sender;

@end

