//
//  EDTableViewCell.h
//  EnglishDict
//
//  Created by Arda on 11/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DictionaryManager.h"
@interface EDTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *initialLabel;
@property (weak, nonatomic) IBOutlet UILabel *wordLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;
- (void)prepareCell:(NSString *)word;

@end
