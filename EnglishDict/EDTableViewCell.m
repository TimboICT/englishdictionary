//
//  EDTableViewCell.m
//  EnglishDict
//
//  Created by Arda on 11/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "EDTableViewCell.h"

@implementation EDTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareCell:(NSString *)word
{
    if(word.length>0)
    {
        NSString *character = [[word substringWithRange:NSMakeRange(0, 1)] uppercaseString];
        [_initialLabel setText:character];
        [_wordLabel setText:word];
        
        
        [_bgView.layer setCornerRadius:5.0];
    }
}

@end
