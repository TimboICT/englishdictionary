//
//  LeftMenuViewController.h
//  EnglishDict
//
//  Created by Arda on 11/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "MainViewController.h"

@interface LeftMenuViewController : MainViewController

- (IBAction)dictionaryClicked:(id)sender;
- (IBAction)settingsClicked:(id)sender;


@end
