//
//  GCSResult.m
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "GCSResult.h"

@implementation GCSResult

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    NSMutableDictionary *paths = [[NSMutableDictionary alloc] init];
    
    [paths addEntriesFromDictionary:@{
                                      @"items":@"items"
                                      }];
    return paths;
}

+ (NSValueTransformer *)itemsJSONTransformer
{
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[GCSItem class]];
}

@end
