//
//  EDValues.h
//  EnglishDict
//
//  Created by Arda Doğantemur on 10/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EDValues : NSObject

+ (NSString *)dictionaryFileName;
+ (int)maxCount;
+ (int)hashDepth;
+ (double)searchDelay;
+ (NSString *)googleCustomSearchApiKey;
+ (NSString *)googleCustomSearchCxKeyGoogle;
+ (NSString *)googleCustomSearchURL;
+ (NSString *)googleCustomSearchCxKeyWiki;

@end
