//
//  SettingsViewController.m
//  EnglishDict
//
//  Created by Arda Doğantemur on 14/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "SettingsViewController.h"
#import "DictionaryManager.h"
@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    double delay = [[DictionaryManager sharedManager] searchDelay];
    [_stepperLabel setText:[NSString stringWithFormat:@"%.2f sec",delay]];
    
    [_stepper setValue:delay/0.1];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)menuClicked:(id)sender
{
    [DELEGATE.sideMenuViewController presentLeftMenuViewController];

}

- (IBAction)stepperValueChanged:(UIStepper *)sender
{
    double value = [sender value];
    [_stepperLabel setText:[NSString stringWithFormat:@"%.2f sec",value*0.1]];
    [[DictionaryManager sharedManager] setSearchDelay:value*0.1];
}

@end
