//
//  LeftMenuViewController.m
//  EnglishDict
//
//  Created by Arda on 11/02/15.
//  Copyright (c) 2015 Timboict. All rights reserved.
//

#import "LeftMenuViewController.h"

@interface LeftMenuViewController ()

@end

@implementation LeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)dictionaryClicked:(id)sender
{
    UINavigationController *navigationController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"mainNavigation"];
    [DELEGATE.sideMenuViewController setContentViewController:navigationController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)settingsClicked:(id)sender
{
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Settings" bundle:nil] instantiateViewControllerWithIdentifier:@"settingsNavigation"];
    [DELEGATE.sideMenuViewController setContentViewController:vc animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

@end
